<?php

namespace ReefExtra\FontAwesome5;

use \Reef\Extension\Extension;

/**
 * Font Awesome 5 icon set
 */
class FontAwesome5 extends Extension {
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'reef-extra:fontawesome5';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'style.css',
				'view' => 'all',
			],
			[
				'type' => 'remote',
				'path' => 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css',
				'view' => 'all',
				'name' => 'fontawesome5',
				'integrity' => 'sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=',
			],
		];
	}

    /**
     * @inherit
     */
    public function getJS() : array {
        return [
            [
                'type' => 'remote',
                'path' => 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js',
                'view' => 'all',
                'name' => 'fontawesome5',
                'integrity' => 'sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=',
            ],
        ];
    }
	
}
